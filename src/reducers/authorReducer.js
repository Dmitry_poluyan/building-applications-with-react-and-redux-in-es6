import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function authorReducer(state = initialState.authors, action) {
  console.log('reducers:authorReducer');

  switch (action.type) {
    case types.LOAD_AUTHORS_SUCCESS:
      console.log('reducers:authorReducer:LOAD_AUTHORS_SUCCESS');

      return action.authors;
    
    default:
      console.log('reducers:authorReducer:default');

      return state;
  }
}
