import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function courseReducer(state = initialState.courses, action) {
  console.log('reducers:courseReducer');

  switch (action.type) {
    case types.LOAD_COURSES_SUCCESS:
      console.log('reducers:courseReducer:LOAD_COURSES_SUCCESS');

      return action.courses;

    case types.CREATE_COURSE_SUCCESS:
      console.log('reducers:courseReducer:CREATE_COURSES_SUCCESS');

      return [
        ...state,
        Object.assign({}, action.course)
      ];

    case types.UPDATE_COURSE_SUCCESS:
      console.log('reducers:courseReducer:UPDATE_COURSES_SUCCESS');

      return [
        ...state.filter(course => course.id !== action.course.id),
        Object.assign({}, action.course)
      ];

    default:
      console.log('reducers:courseReducer:default');

      return state;
  }
}
