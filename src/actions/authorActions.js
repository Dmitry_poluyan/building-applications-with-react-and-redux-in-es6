import AuthorApi from '../api/mockAuthorApi';
import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';

export function loadAuthorsSuccess(authors) {
  console.log('authorActions:loadAuthorsSuccess');

  return {
    type: types.LOAD_AUTHORS_SUCCESS,
    authors: authors
  }
}

export function loadAuthors() {
  console.log('authorActions:loadAuthors');

  return dispatch => {
    dispatch(beginAjaxCall());
    
    return AuthorApi
      .getAllAuthors()
      .then(authors => {
        dispatch(loadAuthorsSuccess(authors));
      }).catch(error => {
        dispatch(ajaxCallError(error));
        throw(error);
      });
  };
}
