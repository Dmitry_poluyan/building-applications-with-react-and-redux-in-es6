import * as types from './actionTypes';
import courseApi from '../api/mockCourseApi';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';

export function loadCoursesSuccess(courses) {
  console.log('courseActions:loadCoursesSuccess');

  return {
    type: types.LOAD_COURSES_SUCCESS,
    courses: courses
  };
}

export function createCourseSuccess(course) {
  console.log('courseActions:createCoursesSuccess');

  return {
    type: types.CREATE_COURSE_SUCCESS,
    course: course
  };
}

export function updateCourseSuccess(course) {
  console.log('courseActions:updateCoursesSuccess');

  return {
    type: types.UPDATE_COURSE_SUCCESS,
    course: course
  };
}

export function loadCourses(course) {
  console.log('courseActions:loadCourses');

  return function (dispatch) {
    dispatch(beginAjaxCall());

    return courseApi
      .getAllCourses()
      .then(courses => {
        dispatch(loadCoursesSuccess(courses));
      }).catch(error => {
        dispatch(ajaxCallError(error));
        throw(error);
      });
  };
}

export function saveCourse(course) {
  console.log('courseActions:saveCourse');

  return function (dispatch, getState) {
    dispatch(beginAjaxCall());

    return courseApi
      .saveCourse(course)
      .then(course => {
        course.id
          ? dispatch(updateCourseSuccess(course))
          : dispatch(createCourseSuccess(course));
      }).catch(error => {
        dispatch(ajaxCallError(error));
        throw(error);
      });
  };
}
