import * as types from './actionTypes';

export function beginAjaxCall() {
  console.log('ajaxStatusActions:beginAjaxCall');
  
  return {type: types.BEGIN_AJAX_CALL};
}

export function ajaxCallError() {
  console.log('ajaxStatusActions:ajaxCallError');
  
  return {type: types.AJAX_CALL_ERROR};
}
