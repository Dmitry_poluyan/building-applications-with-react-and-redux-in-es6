import React, {PropTypes} from 'react';

class LoadingDots extends React.Component {
  constructor(props, context) {
    console.log('components:LoadingDots:constructor');

    super(props, context);

    this.state = {frame: 1};
  }

  componentDidMount() {
    console.log('components:LoadingDots:componentDidMount');
    
    this.interval = setInterval(() => {
      this.setState({ //eslint-disable-line react/no-did-mount-set-state
        frame: this.state.frame + 1
      });
    }, this.props.interval);
  }

  componentWillUnmount() {
    console.log('components:LoadingDots:componentWillUnmount');
    
    clearInterval(this.interval);
  }

  render() {
    console.log('components:LoadingDots:render');
    
    let dots = this.state.frame % (this.props.dots + 1);
    let text = '';
    while (dots > 0) {
      text += '.';
      dots--;
    }
    return <span {...this.props}>{text}&nbsp;</span>
  }
}

LoadingDots.defaultProps = {
  interval: 300, dots: 3
};

LoadingDots.proptypes = {
  interval: PropTypes.number,
  dots: PropTypes.number
};

export default LoadingDots;
