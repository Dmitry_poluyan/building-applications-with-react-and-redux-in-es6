import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as courseActions from '../../actions/courseActions';
import CourseList from './CourseList';
import {browserHistory} from 'react-router';

class CoursesPage extends React.Component {
  constructor(props, context) {
    console.log('components:CoursesPage:constructor');

    super(props, context);

    this.redirectToAddCoursePage = this.redirectToAddCoursePage.bind(this);
  }

  redirectToAddCoursePage() {
    console.log('components:CoursesPage:redirectToAddCoursePage');

    browserHistory.push('/course');
  }

  render() {
    console.log('components:CoursesPage:render');

    const {courses} = this.props;

    return (
      <div>
        <h1>Courses</h1>
        <input
          type="submit"
          value="Add course"
          className="btn btn-primary"
          onClick={this.redirectToAddCoursePage}
        />
        <CourseList courses={courses}/>
      </div>
    );
  }
}

CoursesPage.propTypes = {
  courses: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  console.log('components:CoursesPage:mapStateToProps');

  return {
    courses: state.courses
  };
}

function mapDispatchToProps(dispatch) {
  console.log('components:CoursesPage:mapDispatchToProps');

  return {
    // createCourse: (course) => {
    //   dispatch(courseActions.createCourse(course))
    // }
    actions: bindActionCreators(courseActions, dispatch)
  };
}

const connectedStateAndProps = connect(mapStateToProps, mapDispatchToProps);
export default connectedStateAndProps(CoursesPage);


// {this.props.courses.map(this.courseRow)}

// <h2>Add course</h2>
// <input type="text"
// onChange={this.onTitleChange}
// value={this.state.course.title}
// />
// <input type="submit"
// value="Save"
// onClick={this.onClickSave}
// />

// onTitleChange(event) {
//   console.log('components:CoursesPage:onTitleChange');
//
//   const course = this.state.course;
//   course.title = event.target.value;
//   this.setState({course: course});
// }

// onClickSave() {
//   console.log('components:CoursesPage:onClickSave');
//
//   // alert(`Saving ${this.state.course.title}`);
//   this.props.actions.createCourse(this.state.course);
// }

// courseRow(course, index) {
//   console.log('components:CoursesPage:courseRow');
//
//   return (
//     <div key={index}>
//       {course.title}
//     </div>
//   );
// }
